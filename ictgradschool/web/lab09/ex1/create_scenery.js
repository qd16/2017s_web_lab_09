const backgrounds = [
    "background1.jpg",
    "background2.jpg",
    "background3.jpg",
    "background4.jpg",
    "background5.jpg",
    "background6.gif",
];


$(document).ready(() => {
    // radio button change bg
    $("#choose-background input").on('change', function () {
        bgUrl = "../images/" + backgrounds[this.id.slice(-1) - 1];
        $("#background").attr("src", bgUrl);
    });

    // only show default checked img
    $(".dolphin").each(function () {
        if (!$("#check-" + this.id).prop('checked')) {
            $(this).hide();
        }
    });

    // checkbox toggle img
    $("#toggle-components input").on("change", function () {
        let dolphin = this.id.slice(-8);
        console.log($(this));
        if ($(this).prop('checked'))
            $("#" + dolphin).show()
        else
            $("#" + dolphin).hide()
    });


    $("#size-control").slider(
        {
            min: 0,
            max: 500,
            value: 100,
            slide: function (event, ui) {
                $("#container").css("transform", 'scale(' + ui.value/100 + ')');
                // $("#background").css("transform", 'scale(' + ui.value/100 + ')');
                // $(this).siblings().prev().val(ui.value);
                $(this).prev().prev().text("size:" + ui.value/100);
              }
        }
);

console.log(("#size-control").slider(value));


});